package br.edu.facisa.cwf.example5.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.facisa.cwf.example5.domain.Papel;

public interface PapelRepository extends JpaRepository<Papel, String> {
	
	public Papel findByTipo(String tipo);
	
}
