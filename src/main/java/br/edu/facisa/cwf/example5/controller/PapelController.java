package br.edu.facisa.cwf.example5.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.facisa.cwf.example5.domain.Papel;
import br.edu.facisa.cwf.example5.service.PapelService;

@RestController
public class PapelController {
	
	private final PapelService papelService;
	
	@Autowired
	public PapelController(final PapelService papelService) {
		this.papelService = papelService;
	}
	
	@RequestMapping(value="/papel", method = RequestMethod.GET)
	public ResponseEntity< List<Papel> > listAllUsers() {
		return new ResponseEntity< List<Papel> >
		(papelService.listAllPapeis(), HttpStatus.OK);
	}

	@RequestMapping(value = "/papel/{id}",method = RequestMethod.GET)
	public ResponseEntity<Papel> getUser(@PathVariable String id) {
		
		Papel papel = papelService.getById(id);
		
		return papel == null ? 
				new ResponseEntity<Papel>(HttpStatus.NOT_FOUND) : 
					new ResponseEntity<Papel>(papel, HttpStatus.OK);
	}
	
	@RequestMapping(value="/papel", method = RequestMethod.POST)
	public ResponseEntity<String> createUser(@RequestBody Papel papel) {

		try {
			papelService.save(papel);
			return new ResponseEntity<String>(HttpStatus.CREATED);

		 } catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/papel", method = RequestMethod.PUT)
	public ResponseEntity<String> updateUser(@RequestBody Papel papel) {

		try {
			papelService.updatePapel(papel);
			return new ResponseEntity<String>(HttpStatus.OK);

		 } catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/papel/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<Papel> deleteUser(@PathVariable String id) {
		
		papelService.deletePapel(id);

		return new ResponseEntity<Papel>(HttpStatus.OK);
	}

	public PapelService getUserService() {
		return papelService;
	}
	
}
