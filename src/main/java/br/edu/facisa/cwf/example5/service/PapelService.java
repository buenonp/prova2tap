package br.edu.facisa.cwf.example5.service;

import java.util.List;

import br.edu.facisa.cwf.example5.domain.Papel;


public interface PapelService {

	Papel save(Papel papel);

	Papel getById(String id);
	
	List<Papel> listAllPapeis();
	
	Papel updatePapel(Papel papel);
	
	void deletePapel(String id);

}
