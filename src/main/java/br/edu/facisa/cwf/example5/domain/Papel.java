package br.edu.facisa.cwf.example5.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Papel implements Serializable{
	
	@Id @GeneratedValue
	private String id;
	private String tipo;
	private String cor;
	private double altura;
	private double largura;
	
	public Papel() {
		
	}
	
	public Papel(String id, String tipo, String cor, double altura, double largura) {
		this.id = id;
		this.tipo = tipo;
		this.cor = cor;
		this.altura = altura;
		this.largura = largura;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

}
