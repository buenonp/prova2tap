package br.edu.facisa.cwf.example5.service;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import br.edu.facisa.cwf.example5.domain.Papel;
import br.edu.facisa.cwf.example5.repository.PapelRepository;

@Service
@Validated
public class PapelServiceImpl implements PapelService{
	
	private final PapelRepository repository;
	
	@Autowired
	public PapelServiceImpl(final PapelRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public Papel save(@NotNull @Valid final Papel papel) {
		return repository.save(papel);
	}

	public Papel getById(String id) {
		return repository.findOne(id);
	}

	public List<Papel> listAllPapeis() {
		return repository.findAll();
	}
	
	@Transactional
	public Papel updatePapel(@NotNull @Valid final Papel papel) {
		return repository.save(papel);
	}

	public void deletePapel(String id) {
		repository.delete(id);
	}
	
	public PapelRepository getRepository() {
		return repository;
	}

}
